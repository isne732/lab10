#include <iostream>
#include "list.h"
using namespace std;
void main()
{
	int value1, value2;
	List list;
	list.headPush(3);
	list.headPush(2);
	list.headPush(1);
	list.tailPush(4);
	list.tailPush(5);
	list.tailPush(9);

	list.Display();
	value1 = list.headPop();
	value2 = list.tailPop();
	cout << endl;
	list.Display();
	cout << endl;
	cout << "The head number that we delete is:   " << value1 << endl;
	list.Display();
	cout << endl;
	cout << "The tail number that we delete is: " << value2 << endl;
	if (list.isInList(3) == true) {
		cout << "there is in the list !! " << endl;
	}
	else {
		cout << "there is not in the list !! " << endl;
	}

	system("pause");
	list.Display();
}

