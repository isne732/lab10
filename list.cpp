#include <iostream>
#include "list.h"

using namespace std;
List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}



void List::headPush(int info) {
	Node* t = new Node(info);//make new node equal t

	if (head == 0) {//only one number

		head = t;//make new pointer equal to head
		tail = t;//make new pointer equal to tail
	}
	else {//more than one number
		t->next = head;
		head->prev = t;// new element is must be at head
	}


}

void List::tailPush(int info) {
	Node* p = new Node(info);
	if (tail->next==NULL) {
		tail->next = p;
		p->prev;
		tail = p;

	}
	}




void List::Display() {
	Node* r = head;//make new node eqaul to head.



	while (r != NULL) {// Display all number

		cout << r->info;
		cout << " ";
		r = r->next;
	}


}

int List::headPop() {
	Node* p = head; // make new pointer eqaul to head.
	int value = head->info;
	head = head->next;//make head pop.
	delete p;




	return value;
}

int List::tailPop() {
	Node* p = tail;
	int value = p->info;
	tail = tail->prev;

	delete p;

	tail->next = NULL;

	return value;

}

bool List::isInList(int info) {
	Node* p = head;
	while (p->next != NULL) {// check every elements.
		if (p->info == info)

			return true;

		else {
			p = p->next;
		}

	}
	return false;
}

void deleteNode(struct Node** head_ref, int key)
{
	// Store head node 
	struct Node* temp = *head_ref, * prev;

	// If head node itself holds the key to be deleted 
	if (temp != NULL && temp->info == key)
	{
		*head_ref = temp->next;   // Changed head 
		free(temp);               // free old head 
		return;
	}

	
	while (temp != NULL && temp->info != key)
	{
		prev = temp;
		temp = temp->next;
	}

	
	if (temp == NULL) return;

	prev->next = temp->next;

	free(temp);  // Free memory 
}